<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use App\Http\Controllers\Api\ApiController;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Filter for NotFoundHttpException
        if ($exception instanceOf \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $apiController = new ApiController();
            
            // If an API endpoint was not found we return the message related to the API
            if (str_contains($request->getRequestUri(),"api/v1")) {
                return $apiController->respondNotFound("The requested endpoint doesn't exist");
            }
            
            return response("Hey there, you are lost, this is an API Server");
        }

        return parent::render($request, $exception);
    }
}
