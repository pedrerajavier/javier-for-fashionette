<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TvShowsController extends ApiController
{
    /** 
     * This function will make an api call to the third party service TVMaze forwarding
     * the user query to it. It will analyze the respone and this will be filtered with
     * non-case sensitive and non-typo tolerant values.
     * 
     * @param  Illuminate\Http\Request $request
     * @return json
     */
    public function index(Request $request)
    {
        try {
            $rules = ["query" => "required|min:3|max:20|alpha_num"];
            $data = $request->only('query');
            $validation = \Validator::make($data, $rules);
            
            if ($validation->fails()) {
                return $this->respondValidationErrors($validation->errors()->get('query'));
            }

            $userQuery = strtolower($request["query"]);

            // User queries will be cached 60 minutes in order to avoid extra calls to the third party API
            $resultsCollection = \Cache::remember("tv_shows_query_$userQuery", 60, function() use($userQuery){
                $response = \Curl::to('http://api.tvmaze.com/search/shows')
                    ->withTimeout(5)
                    ->returnResponseObject()
                    ->withData(['q' => $userQuery])
                    ->get();

                // Make a collection for a better filter and map later
                $resultsCollection = collect(json_decode($response->content));

                // Filter typo and case here and then map the results
                return $resultsCollection->filter(function($result) use($userQuery) {
                    return str_contains(strtolower($result->show->name), $userQuery);
                })->map(function($result) { 
                    return [
                        "name"  => $result->show->name, 
                        "score" => $result->score
                    ];
                })->values();
            });
            
            if ($resultsCollection->count() <= 0) {
                return $this->respondNotFound("No results matching for '{$userQuery}'");
            }

            return $this->respondSuccess($resultsCollection);
        } catch (\Exception $e) {
            return $this->respondError($e);
        }
    }
}
