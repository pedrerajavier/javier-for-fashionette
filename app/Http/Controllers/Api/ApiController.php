<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
	
class ApiController extends Controller
{
    const OK = "OK";
    const ERROR = "ERROR";

    protected $statusCode = 200;

    public function getStatusCode()
    {
    	return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
    	$this->statusCode = $statusCode;

    	return $this;
    }

    public function respond($response, $headers = array())
    {
    	return response()->json($response, $this->getStatusCode(), $headers, JSON_UNESCAPED_UNICODE);
    }

    public function respondSuccess($data)
    {
        $response = [
            'status'          => self::OK,
            'data'            => $data,
        ];

        return $this->respond($response);
    }

    public function respondError($error = null)
    {
        if ($error instanceOf \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return $this->respondNotFound();
        }

        // Avoid exception to be shown when project on production
        if (app('env') == 'production' || $error === null) {
            $error = 'Ups! We are sorry something went wrong';
        } else {
            $error = $error instanceOf \Exception
                ? $error->getMessage()
                : $error;
        }

        return $this->setStatusCode(500)->respond([
            'status' => self::ERROR,
            'info'   => $error,
        ]);
    }

    public function respondValidationErrors($validationErrors = [])
    {
        $data['errors'] = $validationErrors;

        return $this->setStatusCode(400)->respond([
            'status' => self::ERROR,
            'info'   => 'Validation errors',
            'data'   => $data,
        ]);
    }

    public function respondNotFound($info = null)
    {
        $info = $info ? : 'Resource not found';

        return $this->setStatusCode(404)->respond([
            'status' => self::ERROR,
            'info'   => $info
        ]);
    }
}