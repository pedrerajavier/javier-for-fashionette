<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class TvShowsTest extends TestCase
{
    /**
     * Here we check validation rules of the endpoint
     *
     * @return void
     */
    public function testValidationRules()
    {
        // Test without query string
        $response = $this->json('GET', '/api/v1/search');
        $response->assertStatus(400)
            ->assertJsonFragment(['info' => 'Validation errors'])
            ->assertJsonFragment(['errors' => ['The query field is required.']]);

        // Minlength test
        $response = $this->json('GET', '/api/v1/search', ['query' => 'fl']);
        $response->assertStatus(400)
            ->assertJsonFragment(['info' => 'Validation errors'])
            ->assertJsonFragment(['errors' => ['The query must be at least 3 characters.']]);

        // Maxlength test
        $response = $this->json('GET', '/api/v1/search', ['query' => str_random(21)]);
        $response->assertStatus(400)
            ->assertJsonFragment(['info' => 'Validation errors'])
            ->assertJsonFragment(['errors' => ['The query may not be greater than 20 characters.']]);

        // Alphanumeric test
        $response = $this->json('GET', '/api/v1/search', ['query' => "%/_"]);
        $response->assertStatus(400)
            ->assertJsonFragment(['info' => 'Validation errors'])
            ->assertJsonFragment(['errors' => ['The query may only contain letters and numbers.']]);
    }

    /**
     * Here we check the success response
     *
     * @return void
     */
    public function testSuccessResponse()
    {
        $response = $this->json('GET', '/api/v1/search', ['query' => 'flash']);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    ['name', 'score']
                ]
            ]);
    }
}
