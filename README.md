# **Javier Pedrera for Fashionette**

The test has been developed with Laravel. A [curl package](https://github.com/ixudra/curl "title" target="_blank") has been included for making the code more readable instead of using traditional curl commands. 

In order to run this project properly please follow these steps:
 
- copy and modify *.env.example* to *.env*
 
- run ``$ composer install``
 
- start server with ``$ php artisan serve`` and make the desired calls to **api/v1/search** with **query** as parameter. Example: _http://localhost:8000/api/v1/search?query=flash_

 
For tests execution you can do in the root project folder ``$ ./vendor/bin/phpunit``
 
# **Main Files**


**Controllers**

- app/Http/Controllers/Api/TvShowsController.php

- app/Http/Controllers/Api/ApiController.php
  

**Tests**

 - tests/Feature/TvShowsTest.php


**Exceptions**

 - app/Exceptions/Handler.php